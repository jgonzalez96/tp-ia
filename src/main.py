import numpy as np
from classifier import Classifier

# Lectura de datos
x = np.genfromtxt('../in/X.csv', delimiter=',')
y = np.genfromtxt('../in/Y.csv', delimiter=',')
y = y.reshape(y.shape[0], 1)

# Entrenamiento
classifier = Classifier()
classifier.fit(x=x, y=y, epochs=50)

# Clasificación
result = classifier.predict(x)

# Evaluación
accuracy = classifier.calculate_accuracy(x, y)
print('Accuracy: {}'.format(accuracy))

# Guardar resultados
x_test = np.genfromtxt('../in/X_test.csv', delimiter=',')
result_test = classifier.predict(x_test)
np.savetxt('../out/Y_test.csv', result_test, delimiter=',')
classifier.save_params()
