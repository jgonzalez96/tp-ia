import pickle

import numpy as np

from activation_function import ActivationFunction


class Classifier:

    def __init__(self, input_layer_neurons=4, hidden_layer_neurons=128, output_layer_neurons=1, learning_rate=.35,
                 activation_function=ActivationFunction.logistic, activation_function_d=ActivationFunction.logistic_d):
        self.input_layer_neurons = input_layer_neurons
        self.hidden_layer_neurons = hidden_layer_neurons
        self.output_layer_neurons = output_layer_neurons
        self.w1 = np.random.uniform(-.1, .1, size=(input_layer_neurons + 1, hidden_layer_neurons))
        self.w2 = np.random.uniform(-.1, .1, size=(hidden_layer_neurons + 1, output_layer_neurons))
        self.learning_rate = learning_rate
        self.activation_function = activation_function
        self.activation_function_d = activation_function_d

    def fit(self, x, y, epochs=10):
        for epoch in range(epochs):
            print('Epoch {}'.format(epoch + 1))
            for i in range(x.shape[0]):
                xi = x[i, :]
                h = self.__get_output(xi, self.w1)
                o = self.__get_output(h, self.w2)

                error_2 = y[i, :] - o
                act_d = np.apply_along_axis(self.activation_function_d, 0, o)
                error_2 = error_2 * act_d

                error_1 = np.matmul(error_2, np.transpose(self.w2[1:, :]))
                act_d = np.apply_along_axis(self.activation_function_d, 0, h)
                error_1 = error_1 * act_d

                xi_aux = np.transpose(np.reshape(np.hstack((np.array([1]), xi)), (1, xi.shape[0] + 1)))
                delta_w1 = self.learning_rate * np.matmul(xi_aux, error_1.reshape(1, error_1.shape[0]))
                self.w1 = self.w1 + delta_w1

                h_aux = np.transpose(np.reshape(np.hstack((np.array([1]), h)), (1, h.shape[0] + 1)))
                delta_w2 = self.learning_rate * np.matmul(h_aux, error_2.reshape(1, error_2.shape[0]))
                self.w2 = self.w2 + delta_w2

    def predict(self, x):
        output = np.array([])
        for i in range(x.shape[0]):
            xi = x[i, :]
            h = self.__get_output(xi, self.w1)
            o = self.__get_output(h, self.w2)
            o = np.apply_along_axis(lambda a: 1 if a > .5 else 0, 0, o)
            output = np.append(output, o)
        return output

    def __get_output(self, x, w):
        x = np.hstack((np.array([1]), x))
        net = np.matmul(x, w)
        return np.apply_along_axis(self.activation_function, 0, net)

    def save_params(self, name='w'):
        pickle.dump(self.w1, open('../out/{}1.pickle'.format(name), 'wb'))
        pickle.dump(self.w2, open('../out/{}2.pickle'.format(name), 'wb'))

    def load_params(self, name='w'):
        self.w1 = pickle.load(open('../out/{}1.pickle'.format(name), 'rb'))
        self.w2 = pickle.load(open('../out/{}2.pickle'.format(name), 'rb'))

    def __generate_confusion_matrix(self, x, y, u=.5):
        vp, fp, vn, fn = (0, 0, 0, 0)
        for i in range(x.shape[0]):
            xi = x[i, :]
            h = self.__get_output(xi, self.w1)
            o = self.__get_output(h, self.w2)
            if o > u:
                if y[i] > 0:
                    vp += 1
                else:
                    fp += 1
            else:
                if y[i] > 0:
                    fn += 1
                else:
                    vn += 1
        return vp, fp, vn, fn

    def calculate_accuracy(self, x, y, u=.5):
        vp, fp, vn, fn = self.__generate_confusion_matrix(x, y, u)
        accuracy = 1.0 * (vp + vn) / (vp + vn + fp + fn)
        print(vp, fp, vn, fn)
        return accuracy
