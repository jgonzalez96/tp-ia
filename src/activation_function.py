import numpy as np


class ActivationFunction:

    @staticmethod
    def logistic(value):
        return 1.0 / (1 + np.exp(1) ** (-1.0 * value))

    @staticmethod
    def logistic_d(value):
        return value * (1 - value)
